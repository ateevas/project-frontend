import Vue from "vue";
import Antd from "ant-design-vue/lib";
import moment from "moment";

Vue.prototype.$moment = moment;
Vue.use(Antd);
